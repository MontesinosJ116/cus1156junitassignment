package JunitPrac;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MyPetTest {
	
	MyPet jen;
	MyPet ken;

	@BeforeEach
	void setUp() throws Exception {
		jen = new MyPet ("Jennifer", 2, "Apple");
		ken = new MyPet ("Kenneth", 3, "granola bars");
	}

	@Test
	public void testIsHealthy() {
		assertFalse(jen.isHealthy());
		assertTrue(ken.isHealthy());
		
	}

	@Test
	public void testFitsInCage() {
		assertTrue(jen.fitsInCage(4), "Will it fit");
		assertTrue(ken.fitsInCage(4), "Will it fit");
	}
	
	@Test
	public void testLengthInInches() {
		assertEquals(24, jen.lengthInInches());
		assertEquals(36, ken.lengthInInches());
	}
	

}
